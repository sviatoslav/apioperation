Pod::Spec.new do |s|
  s.name        = "APIOperation"
  s.version     = "0.0.1"
  s.summary     = "APIOperation is a bridge between networking code and model classes"
  s.homepage    = "https://github.com/sviatoslav/apioperation"
  s.license     = { :type => "MIT" }
  s.authors     = { "sviatoslav" => "sviatoslav.yakymiv@gmail.com" }

  s.requires_arc = true
  s.ios.deployment_target = "8.0"
  s.source   = { :git => "https://sviatoslav@bitbucket.org/sviatoslav/apioperation.git", :tag => s.version }
  s.source_files = "Source/*.swift"

  s.frameworks = 'CoreData'

  s.dependency 'Alamofire'
  s.dependency 'MagicalRecord'
  s.dependency 'Operations'
end
