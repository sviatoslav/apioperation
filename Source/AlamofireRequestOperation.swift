// AlamofireRequestOperation.swift
//
// Copyright (c) 2016 Sviatoslav Yakymiv
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Alamofire

class AlamofireRequestOperation: RequestOperation {
    
    private let method: Alamofire.Method
    private let requestData: RequestData
    
    required init(method: Alamofire.Method, requestData: RequestData) {
        self.method = method
        self.requestData = requestData
    }
    
    override func execute() {
        Alamofire.request(self.method, requestData.URL, parameters: requestData.parameters,
            encoding: .JSON, headers: requestData.headerFields)
            .validate(statusCode: 200..<400)
            .responseJSON { response in
                if !self.cancelled {
                    self.result = response.result.value
                    if let code = response.result.error?.code {
                            self.finish(Error(rawValue: code) ?? Error.Unknown)
                    }
                    self.finish()
                }
            }
    }
    
    enum Error: Int, ErrorType {
        case Unknown
        case InputStreamReadFailed           = -6000
        case OutputStreamWriteFailed         = -6001
        case ContentTypeValidationFailed     = -6002
        case StatusCodeValidationFailed      = -6003
        case DataSerializationFailed         = -6004
        case StringSerializationFailed       = -6005
        case JSONSerializationFailed         = -6006
        case PropertyListSerializationFailed = -6007
    }
}
