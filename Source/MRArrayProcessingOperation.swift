// MRArrayProcessingOperation.swift
//
// Copyright (c) 2016 Sviatoslav Yakymiv
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import MagicalRecord

public class MRArrayProcessingOperation<T>: MRResponseProcessingOperation<T> {
    override public func execute() {
        guard let initializableType = T.self as? Initializable.Type,
            container = initializableType.init() as? ContainerType,
            managedObjectClass = container.elementType() as? NSManagedObject.Type else {
                self.finish(MRResponseProcessingOperationError.InvalidGenericType)
                return
        }
        guard let array = self.requirement as? [[NSObject: AnyObject]] else {
            self.finish(MRResponseProcessingOperationError.InvalidRequirementType)
            return
        }
        var managedObjects: [NSManagedObject] = []
        self.saveWithBlock({ (context) -> Void in
            managedObjects = managedObjectClass.MR_importFromArray(array, inContext: context)
        }, completion: {
            let managedObjectsInMainMOC = NSMutableArray()
            managedObjects.forEach {
                let objectID = $0.objectID
                let object = NSManagedObjectContext.MR_defaultContext().objectWithID(objectID)
                managedObjectsInMainMOC.addObject(object)
            }
            self.result = managedObjectsInMainMOC as? T
            self.finish()
        })
    }
}