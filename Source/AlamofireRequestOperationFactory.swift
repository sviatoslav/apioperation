// AlamofireRequestOperationFactory.swift
//
// Copyright (c) 2016 Sviatoslav Yakymiv
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Alamofire

public struct AlamofireRequestOperationFactory: RequestOperationFactory {
    
    public init() {}
    
    public func createRequestOperationWithData(data: RequestData) -> RequestOperation {
        guard let alamofireMethod = self.alamofireMethodWithMethod(data.method) else {
            fatalError("Invalid method")
        }
        return AlamofireRequestOperation(method: alamofireMethod, requestData: data)
    }
    
    public func canCreateRequestOperationWithMethod(method: Method) -> Bool {
        return self.alamofireMethodWithMethod(method) != nil
    }
    
    private func alamofireMethodWithMethod(method: Method) -> Alamofire.Method? {
        switch method {
        case .OPTIONS: return .OPTIONS
        case .GET: return .GET
        case .HEAD: return .HEAD
        case .POST: return .POST
        case .PUT: return .PUT
        case .PATCH: return .PATCH
        case .DELETE: return .DELETE
        case .TRACE: return .TRACE
        case .CONNECT: return .CONNECT
        default: return nil
        }
    }
}