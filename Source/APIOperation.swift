// APIOperation.swift
//
// Copyright (c) 2016 Sviatoslav Yakymiv
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Operations

private struct APIOperationStorage {
    static var defaultOperationConfiguration: ConfigurationType?
    static let defaultOperationQueue: OperationQueue = {
        let queue = OperationQueue()
        return queue
    }()
    static var defaultCompletionQueue = dispatch_get_main_queue()
}

public class APIOperation<T>: GroupOperation, ResultOperationType, APIOperationEnqueueable {
    
    public typealias Result = T?
    public typealias Completion = (Result, [ErrorType]) -> Void
    
    public static var defaultConfiguration: ConfigurationType? {
        get {
            return APIOperationStorage.defaultOperationConfiguration
        }
        set {
            APIOperationStorage.defaultOperationConfiguration = newValue
        }
    }
    
    private var internalConfiguration: ConfigurationType?
    
    public var configuration: ConfigurationType? {
        get {
            return self.internalConfiguration ?? APIOperation.defaultConfiguration
        }
        set {
            self.internalConfiguration = newValue
        }
    }
    
    public static var defaultOperationQueue: OperationQueue {
        return APIOperationStorage.defaultOperationQueue
    }
    
    public static var defaultCompletionQueue: dispatch_queue_t {
        get {
            return APIOperationStorage.defaultCompletionQueue
        }
        set {
            APIOperationStorage.defaultCompletionQueue = newValue
        }
    }
    
    public lazy var completionQueue = APIOperation<Any>.defaultCompletionQueue
    
    private let path: String
    private let completion: Completion?
    
    public var requestData: RequestData
    public var startingOperation: Operation = DelayOperation(interval: -1)
    public var finishingOperation: Operation = DelayOperation(interval: -1)
    public var responsePreprocessingOperation = MappingOperation<AnyObject, AnyObject> {$0}
    
    private var requestOperation: RequestOperation?
    private var responseProcessingOperation: ResponseProcessingOperation<T>?
    
    //http://stackoverflow.com/questions/24161563/swift-compile-error-when-subclassing-nsobject-and-using-generics
    private var results: [T] = []
    public var result: Result {
        get {
            return results.first
        }
        set {
            if let value = newValue {
                results = [value]
            } else {
                results = []
            }
        }
    }

    public init(method: Method, path: String, headerFields: [String : String] = [:], parameters: [String: AnyObject] = [:],
        completion: Completion? = nil) {
        self.requestData = RequestData(method: method, headerFields: headerFields, parameters: parameters)
        self.path = path
        self.completion = completion
        super.init(operations: [self.startingOperation])
    }
    
    override public func operationDidFinish(operation: NSOperation, withErrors errors: [ErrorType]) {
        if !errors.isEmpty {
            self.cancel()
            return
        }
        if operation === self.startingOperation {
            self.startingOperationDidFinish()
        }
        if operation === self.responseProcessingOperation {
            self.result = self.responseProcessingOperation?.result
        }
    }
    
    private func startingOperationDidFinish() {
        guard let configuration = self.configuration else {
            fatalError("Attempt to execute APIOperation without configuration")
        }
        let requestOperationFactory = configuration.requestOperationFactory
        if !requestOperationFactory.canCreateRequestOperationWithMethod(requestData.method) {
            fatalError("Unsupported method for request operation factory")
        }
        self.requestData.URL = configuration.URLForPath(self.path)
        let requestOperation = requestOperationFactory.createRequestOperationWithData(self.requestData)
        requestOperation.addDependency(self.startingOperation)
        self.responsePreprocessingOperation.injectResultFromDependency(requestOperation)
        let responseProcessingOperation = configuration.responseProcessingOperationFactory
            .createResponceProcessingOperationWithType(T.self)
        responseProcessingOperation.injectResultFromDependency(self.responsePreprocessingOperation)
        self.finishingOperation.addDependency(responseProcessingOperation)
        self.addOperations([requestOperation, self.responsePreprocessingOperation,
            responseProcessingOperation, self.finishingOperation])
        
        self.requestOperation = requestOperation
        self.responseProcessingOperation = responseProcessingOperation
    }
    
    override public func finished(errors: [ErrorType]) {
        dispatch_async(self.completionQueue) {
            self.completion?(self.result, errors)
        }
    }
    
    public class func cancelAll() {
        self.defaultOperationQueue.cancelAllOperations()
    }
}
