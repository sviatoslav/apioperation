// ConfigurationType.swift
//
// Copyright (c) 2016 Sviatoslav Yakymiv
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

public protocol ConfigurationType {
    var baseURL: NSURL { get }
    var requestOperationFactory: RequestOperationFactory { get }
    var responseProcessingOperationFactory: ResponseProcessingOperationFactory { get }
    func URLForPath(path: String) -> NSURL
}

public extension ConfigurationType {
    func URLForPath(path: String) -> NSURL {
        if path[path.startIndex] == "/" {
            guard let host = self.baseURL.host else {
                fatalError("Unable to get host from base URL - \(self.baseURL)")
            }
            let originalPath = NSURL(fileURLWithPath: self.baseURL.scheme + "://" + host)
            return originalPath.URLByAppendingPathComponent(path)
        } else {
            return self.baseURL.URLByAppendingPathComponent(path)
        }
    }
}

public struct Configuration: ConfigurationType {
    public var baseURL: NSURL
    public var requestOperationFactory: RequestOperationFactory
    public var responseProcessingOperationFactory: ResponseProcessingOperationFactory
    
    public init(baseURL: NSURL, requestOperationFactory: RequestOperationFactory,
        responseProcessingOperationFactory: ResponseProcessingOperationFactory) {
            self.baseURL = baseURL
            self.requestOperationFactory = requestOperationFactory
            self.responseProcessingOperationFactory = responseProcessingOperationFactory
    }
}
